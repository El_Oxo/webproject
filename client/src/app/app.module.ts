import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { Router } from '@angular/router';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AddViewComponent } from './add-view/add-view.component';
import { StartViewComponent } from './start-view/start-view.component';
import { GetViewComponent } from './get-view/get-view.component';
import { FormsModule, ReactiveFormsModule  }        from '@angular/forms';
import {SchuheService} from './services/schuhe.service';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ImpressumComponent } from './impressum/impressum.component';

@NgModule({
  declarations: [
    AppComponent,
    AddViewComponent,
    StartViewComponent,
    GetViewComponent,
    ImpressumComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
    NgbModule
  ],
  providers: [SchuheService],
  bootstrap: [AppComponent]
})
export class AppModule {  constructor(router: Router) {}}
