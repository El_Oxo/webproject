import { TestBed } from '@angular/core/testing';

import { SchuheService } from './schuhe.service';

describe('SchuheService', () => {
  let service: SchuheService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(SchuheService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
