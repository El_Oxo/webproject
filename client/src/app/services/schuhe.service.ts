import { Injectable } from '@angular/core';
import { schuheModel } from '../models/schuheModel';

@Injectable({
  providedIn: 'root'
})
export class SchuheService {
  schuheList: Array<schuheModel> = ([]);
  anzahl:number =0;

  constructor() { }

  get() {
    return this.schuheList;
  }

  getAnzahl(){
    return this.anzahl;
  }

  add(mname: string, sSize: number, moname: string, pic: string | ArrayBuffer) {
    let newSchuh = new schuheModel();
    newSchuh.mname = mname;
    newSchuh.sSize = sSize;
    newSchuh.moname = moname;
    newSchuh.pic = pic;
    this.anzahl +=1;
    this.schuheList.push(newSchuh);
  }

  update(item: number, mname: string, sSize: number, moname: string, pic: string | ArrayBuffer) {
    this.schuheList[item].mname = mname;
    this.schuheList[item].sSize = sSize;
    this.schuheList[item].moname = moname;
    this.schuheList[item].pic = pic;
  }
  delete(item:number){
   var schuheList2: Array<schuheModel> = ([]);
    this.schuheList.forEach(element => {
      if(this.schuheList.indexOf(element) !== item){
          schuheList2.push(element);
      }
    });
    this.anzahl-=1;
    this.schuheList = schuheList2;
  }
}