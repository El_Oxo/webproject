import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { SchuheService } from '../services/schuhe.service';

@Component({
  selector: 'app-add-view',
  templateUrl: './add-view.component.html',
  styleUrls: ['./add-view.component.scss']
})
export class AddViewComponent implements OnInit {

	submitted = false;
	userForm: FormGroup;
  schuheList: any;
  //Für Bildvorschau Begin
  public imagePath;
  imgURL: any;
  //Für Bildvorschau Ende
  constructor(private formBuilder: FormBuilder, private SchuheServie:SchuheService){}

  ngOnInit(){
  	this.userForm = this.formBuilder.group({
  		mname: ['', Validators.required],
  		sSize: ['', Validators.required],
  		moname: ['', Validators.required],
  		pic: ['', Validators.required]
    });
    this.schuheList = this.SchuheServie.get();
  }
  falscherMarkennaame(){  	return (this.submitted && this.userForm.controls.mname.errors != null);  }
  falscheSchuhgroese(){  	return (this.submitted && this.userForm.controls.sSize.errors != null);  }
  falscherModelname(){  	return (this.submitted && this.userForm.controls.moname.errors != null);  }
  falschesFoto(){ 	return (this.submitted && this.userForm.controls.pic.errors != null);  }

  onSubmit()
  {
  	this.submitted = true;
  	if(this.userForm.invalid == true)
  	{
  		return;
  	}
  	else
  	{
      this.SchuheServie.add(this.userForm.controls["mname"].value,this.userForm.controls["sSize"].value,this.userForm.controls["moname"].value, this.imgURL);
      this.schuheList = this.SchuheServie.get();
      this.resetFormAfterSubmit();
  	}
  }

  resetFormAfterSubmit(){
    this.userForm.reset();
    this.imgURL = null;
    for(var name in this.userForm.controls) {
      this.userForm.controls[name].setErrors(null);
    }
  }

 
  //FÜR ANZEIGEN DES FOTOS
  preview(files) {
    if (files.length === 0){
  
      return;
    }
    var mimeType = files[0].type;
    if (mimeType.match(/image\/*/) == null) {
      return;
    }
    var reader = new FileReader();
    this.imagePath = files;
    reader.readAsDataURL(files[0]); 
    reader.onload = (_event) => { 
      this.imgURL = reader.result; 
     
    }
  }
}
