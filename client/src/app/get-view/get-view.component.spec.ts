import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GetViewComponent } from './get-view.component';

describe('GetViewComponent', () => {
  let component: GetViewComponent;
  let fixture: ComponentFixture<GetViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GetViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GetViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
