import { Component, OnInit } from '@angular/core';
import { SchuheService } from '../services/schuhe.service';
import { NgbModal, ModalDismissReasons, NgbModalOptions } from '@ng-bootstrap/ng-bootstrap';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-get-view',
  templateUrl: './get-view.component.html',
  styleUrls: ['./get-view.component.scss']
})
export class GetViewComponent implements OnInit {

  closeResult: string;
  modalOptions: NgbModalOptions;
  schuheList: any;
  /* Modal1 */
  modalImg: string | ArrayBuffer;
  modalName: string;
  /* Modal2 */
  alterMarkenname: string;
  alteSchuhgr: number;
  alterModelname: string;
  altesBild: string | ArrayBuffer;
  item: number;

  submitted = false;
  userForm: FormGroup;
  imgURL: any;

  constructor(private formBuilder: FormBuilder, private SchuheServie: SchuheService, private modalService: NgbModal) {
    this.modalOptions = {
      backdrop: 'static',
      backdropClass: 'customBackdrop'
    }
  }
  ngOnInit() {
    this.userForm = this.formBuilder.group({
      mname: ['', Validators.required],
      sSize: ['', Validators.required],
      moname: ['', Validators.required],
      pic: ['', Validators.required]
    });
    this.schuheList = this.SchuheServie.get();
  }
  delete(item: number) {
    this.SchuheServie.delete(item);
    this.schuheList = this.SchuheServie.get();
  }
  /* Für das Modal2 neue Eingabe */
  onSubmit() {
    this.submitted = true;
    this.update();
    this.schuheList = this.SchuheServie.get();
    this.resetFormAfterSubmit();
    this.imgURL = null;
    this.modalService.dismissAll();
  }
  update() {
    var mname: string;
    var sSize: number;
    var moname: string;
    var pic: string | ArrayBuffer;
    if (this.userForm.controls["mname"].value == null) { mname = this.alterMarkenname; } else { mname = this.userForm.controls["mname"].value };
    if (this.userForm.controls["sSize"].value == null) { sSize = this.alteSchuhgr; } else { sSize = this.userForm.controls["sSize"].value; }
    if (this.userForm.controls["moname"].value == null) { moname = this.alterModelname; } else { moname = this.userForm.controls["moname"].value; }
    if (this.imgURL == null) { pic = this.altesBild; } else { pic = this.imgURL; }
    this.SchuheServie.update(this.item, mname, sSize, moname, pic);
  }
  resetFormAfterSubmit() {
    this.userForm.reset();
    for (var name in this.userForm.controls) {
      this.userForm.controls[name].setErrors(null);
    }
  }
  //FÜR ANZEIGEN DES FOTOS
  preview(files) {
    if (files.length === 0) {
      return;
    }
    var mimeType = files[0].type;
    if (mimeType.match(/image\/*/) == null) {
      return;
    }
    var reader = new FileReader();
    reader.readAsDataURL(files[0]);
    reader.onload = (_event) => {
      this.imgURL = reader.result;
    }
  }
  /*   Für das MODAL */
  open(content, item?: number, pic?: string | ArrayBuffer, moname?: string) {
    if (pic !== null && moname !== null) {
      this.setModal(pic, moname);
    }
    if (item !== null) {
      this.item = item;
      this.alterMarkenname = this.schuheList[item].mname;
      this.alteSchuhgr = this.schuheList[item].sSize;
      this.alterModelname = this.schuheList[item].moname;
      this.altesBild = this.schuheList[item].pic;
      this.imgURL = this.altesBild;
      this.resetFormAfterSubmit();
    }
    this.modalService.open(content, this.modalOptions).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }
  setModal(pic: string | ArrayBuffer, moname: string) {
    this.modalImg = pic;
    this.modalName = moname;
  }
  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }

}
