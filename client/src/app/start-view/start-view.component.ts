import { Component, OnInit } from '@angular/core';
import { SchuheService } from '../services/schuhe.service';
import { NgbModal, ModalDismissReasons, NgbModalOptions } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-start-view',
  templateUrl: './start-view.component.html',
  styleUrls: ['./start-view.component.scss']
})
export class StartViewComponent implements OnInit {

  closeResult: string;
  modalOptions: NgbModalOptions;

  schuheList: any;
  mname: string;
  sSize: number;
  moname: string;
  pic: string | ArrayBuffer;
  message: string = "";
  fehler: boolean = true;
  btnLabel: string = "Klick hier um es herrauszufinden";

  constructor(private SchuheServie: SchuheService, private modalService: NgbModal) {
  this.modalOptions = {
    backdrop: 'static',
    backdropClass: 'customBackdrop'
  }
  }

  ngOnInit() { this.schuheList = this.SchuheServie.get(); }

  getRandomSchuh() {
    if (this.SchuheServie.getAnzahl() == 0) {
      this.message = "Du hast noch keine Schuhe hinzugefügt."
    }
    else {
      var randomZahl = Math.floor(Math.random() * Math.floor(this.SchuheServie.getAnzahl()));
      this.mname = this.schuheList[randomZahl].mname;
      this.sSize = this.schuheList[randomZahl].sSize;
      this.moname = this.schuheList[randomZahl].moname;
      this.pic = this.schuheList[randomZahl].pic;
      this.btnLabel = "Klick mich nochmal, wenn der Schuhe dir nicht gefällt.";
      this.fehler = false;
     
    }
  }


  open(content) {
    this.modalService.open(content, this.modalOptions).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }
  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }
}

