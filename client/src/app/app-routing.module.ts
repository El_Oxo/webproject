import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AddViewComponent } from './add-view/add-view.component';
import { StartViewComponent } from './start-view/start-view.component';
import { GetViewComponent } from './get-view/get-view.component';
import { ImpressumComponent} from './impressum/impressum.component';

const routes: Routes = [{
  path:'addView',
  component: AddViewComponent
},{
  path: 'getView',
  component:GetViewComponent
},{
  path: '',
  component: StartViewComponent
},{
  path:'startView',
  component: StartViewComponent
},{
  path:'impressum',
  component: ImpressumComponent
}];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
